<?php $__env->startSection('title', 'Dashboard '); ?>

<?php $__env->startSection('content'); ?>

<div class="col-md-9">
  <div class="dash-content">
    <div class="row no-margin">
      <div class="col-md-12">
        <h4 class="page-title"><?php echo app('translator')->getFromJson('user.ride.ride_now'); ?></h4>
      </div>
    </div>
    <?php echo $__env->make('common.notify', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row no-margin">
      <div class="col-md-6">
        <div class="text-right col-12">
          <button class="btn btn-sm fare-btn" style="background-color: #ff5e00; border-radius: 40px;" onclick="adicionarParadaInput()"> <?php echo app('translator')->getFromJson('Adicionar + Entregas'); ?> <i class="fa fa-plus-circle"></i></button>
        </div>
        <form action="<?php echo e(url('confirm/ride')); ?>" method="GET" onkeypress="return disableEnterKey(event);">
          <div class="input-group dash-form">
            <input type="text" class="form-control" id="origin-input" name="s_address"  placeholder="Local de partida">
          </div>
          <?php foreach ([1,2,3,4,5] as $key => $pa): ?>
            <div class="input-group dash-form paradas hide" id="parada-div-<?php echo e($pa); ?>">
              <input type="text" class="form-control " id="parada-input-<?php echo e($pa); ?>" name="paradas[<?php echo e($pa); ?>][descricao]"  placeholder="<?php echo e($pa); ?>º Parada (colocar endereço) ">
              <input type="text" class="form-control form-control-sm" id="parada-input-<?php echo e($pa); ?>" name="paradas[<?php echo e($pa); ?>][cliente]"  placeholder="Quem vai receber? ">
            </div>
          <?php endforeach ?>
          <div class="input-group dash-form">
            <input type="text" class="form-control" id="destination-input" name="d_address"  placeholder="Destino Final" >

            <input type="text" style="height: 40px;" class="form-control form-control-sm" id="cliente-input" name="cliente_volta"  placeholder="Recebedor ou Lojista" >

          </div>

          <input type="hidden" name="s_latitude" id="origin_latitude">
          <input type="hidden" name="s_longitude" id="origin_longitude">
          <input type="hidden" name="d_latitude" id="destination_latitude">
          <input type="hidden" name="d_longitude" id="destination_longitude">

          <?php foreach ([1,2,3,4,5] as $key => $pa): ?>
            <input type="hidden" name="paradas[<?php echo e($pa); ?>][latitude]" id="pp_lat_<?php echo e($pa); ?>">
            <input type="hidden" name="paradas[<?php echo e($pa); ?>][longitude]" id="pp_long_<?php echo e($pa); ?>">
          <?php endforeach ?>

          <input type="hidden" name="current_longitude" id="long">
          <input type="hidden" name="current_latitude" id="lat">

          <div class="car-detail"  style="direction: ltr !important;">

            <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="car-radio">
              <input type="radio" 
              name="service_type"
              value="<?php echo e($service->id); ?>"
              id="service_<?php echo e($service->id); ?>"
              <?php if($loop->first): ?> <?php endif; ?>>

              <label for="service_<?php echo e($service->id); ?>">
                <div class="car-radio-inner" style="width: 250px;height: 10px;">
                  <div class="img"><img src="<?php echo e(image($service->image)); ?>"></div>
                  <div class="name"><span><?php echo e($service->name); ?><p style="font-size: 10px; color:#ffff">(1-<?php echo e($service->capacity); ?>)</p></span>
                  </div>
                </div>
              </label>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


          </div>

          <div class="input-group dash-form" id="hours" >
            <input type="number" class="form-control" id="rental_hours" name="rental_hours"  placeholder="(Horas de aluguel) Quantas horas?" >
          </div>
          <button type="submit"  class="full-primary-btn fare-btn" style="background-color: #ff5e00; border-radius: 40px;"><?php echo app('translator')->getFromJson('user.ride.ride_now'); ?></button>


        </form>
      </div>

      <div class="col-md-6">
        <div class="map-responsive">
          <div id="map" style="width: 100%; height: 450px;"></div>
        </div> 
      </div>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>    
<script type="text/javascript">
  var current_latitude = 13.0574400;
  var current_longitude = 80.2482605;
</script>

<script type="text/javascript">
  $(".drp1").hide();
  $("#drplocat").click(function(){
    $(".drplocat").hide();
    $(".drp1").show()
  });


  if( navigator.geolocation ) {
   navigator.geolocation.getCurrentPosition( success, fail );
 } else {
  console.log('Desculpe, seu navegador não suporta serviços de geolocalização');
  initMap();
}

function success(position)
{
  document.getElementById('long').value = position.coords.longitude;
  document.getElementById('lat').value = position.coords.latitude

  if(position.coords.longitude != "" && position.coords.latitude != ""){
    current_longitude = position.coords.longitude;
    current_latitude = position.coords.latitude;
  }
  initMap();
}

function fail()
{
        // Could not obtain location
        console.log('incapaz de obter a sua localização');
        initMap();
      }
    </script> 


    <script type="text/javascript" src="<?php echo e(asset('asset/js/map.js')); ?>"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo e(Config::get('constants.map_key')); ?>&libraries=places&callback=initMap" defer="" async=""></script>

    <script type="text/javascript">
      function disableEnterKey(e)
      {
        var key;
        if(window.e)
            key = window.e.keyCode; // IE
          else
            key = e.which; // Firefox

          if(key == 13)
            return e.preventDefault();
        }
      </script>
      <script type="text/javascript">
        $(document).ready(function(){
          $("#hours").hide();

          $('input[name=service_type]').change(function(){

            var id =     $('input[name=service_type]:checked').val();

            $.ajax({url: "<?php echo e(url('hour')); ?>/"+id,dataType: "json",
             success: function(data){
                    //console.log(data['calculator']);

                       /*if (data['calculator'] == 'DISTANCEHOUR')
                       $("#hours").show();  
                       else
                         $("#hours").hide(); */
                     }});
          });
        }); 

        setInterval("checkstatus()",3000); 

        function checkstatus(){
          $.ajax({
            url: '/user/incoming',
            dataType: "JSON",
            data:'',
            type: "GET",
            success: function(data){
              if(data.status==1){
                window.location.replace("/dashboard");
              }
            }
          });
        }



      </script>

      <?php $__env->stopSection(); ?>
<?php echo $__env->make('user.layout.base', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>